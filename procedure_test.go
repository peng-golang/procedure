package procedure

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"os"
	"testing"
)

func GetConsoleEncoder() zapcore.Encoder {
	encoderConfig := zap.NewProductionEncoderConfig()
	encoderConfig.EncodeTime = zapcore.ISO8601TimeEncoder
	encoderConfig.EncodeLevel = zapcore.CapitalLevelEncoder
	return zapcore.NewConsoleEncoder(encoderConfig)
}

func GetConsoleLog() *zap.Logger {
	encoder := GetConsoleEncoder()
	writer := zapcore.AddSync(os.Stdout)
	core := zapcore.NewCore(encoder, writer, zapcore.DebugLevel)
	return zap.New(core)
}

type ictx1 interface {
	IProcedureContext
	SetNum(num int)
	GetNum() int
}

type ictx2 interface {
	ictx1
	Add()
}

type ctx1 struct {
	*zap.SugaredLogger
	Num int
}

func (c *ctx1) GetLogger() *zap.SugaredLogger {
	return c.SugaredLogger
}

func (c *ctx1) SetNum(num int) {
	c.Num = num
}

func (c *ctx1) GetNum() int {
	return c.Num
}

func (c *ctx1) GetContextId() string {
	return "test"
}

func (c *ctx1) GetContextString() string {
	return fmt.Sprintf("%v", c)
}

type ctx2 struct {
	*ctx1
}

func (c *ctx2) Add() {
	c.Num++
}

type processAdd struct {
}

func (p *processAdd) GetProcessId() string {
	return GetStructId(p)
}

func (p *processAdd) Process(t ictx1, eventFunc AddEventFunc) error {
	t.SetNum(t.GetNum() + 1)
	eventFunc(testEvent{Num: t.GetNum()})
	return nil
}

type processError struct {
	Err error
}

func (p *processError) GetProcessId() string {
	return "processError"
}

func (p *processError) Process(ctx ictx1, eventFunc AddEventFunc) error {
	return p.Err
}

type testEvent struct {
	Num int
}

func TestExecute(t *testing.T) {
	l := GetConsoleLog().Sugar()
	ctx2 := &ctx2{ctx1: &ctx1{Num: 0, SugaredLogger: l}}
	pro := New[ictx1](ctx2).
		Execute(new(processAdd)).
		Execute(new(processAdd))
	if pro.Err != nil {
		t.Error(pro.Err)
	}

	assert.Equal(t, 2, pro.Ctx.GetNum())
}

func TestGetEventAndRemove(t *testing.T) {
	l := GetConsoleLog().Sugar()
	var ctx ictx1 = &ctx1{Num: 0, SugaredLogger: l}
	pro := New(ctx).
		Execute(&processAdd{}).
		Execute(&processAdd{}).
		Execute(&processAdd{})
	count := 0
	for {
		if e, ok := GetAndRemoveEvents[testEvent](pro); ok {
			l.Infof("get num %d", e.Num)
		} else {
			break
		}
		count++
	}
	assert.Equal(t, 3, count)
}

func TestExecute_Error(t *testing.T) {
	l := GetConsoleLog().Sugar()
	ctx2 := &ctx2{ctx1: &ctx1{Num: 0, SugaredLogger: l}}
	err := fmt.Errorf("process error")
	pro := New[ictx1](ctx2).
		Execute(new(processAdd)).
		Execute(new(processAdd)).
		Execute(&processError{Err: err})

	if pro.Err != nil {
		l.Errorw("execute error", zap.Error(pro.Err))
	}

	assert.Equal(t, 2, pro.Ctx.GetNum())
	assert.Error(t, err, pro.Err)
}
