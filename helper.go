package procedure

import "reflect"

func GetEvents[TEvent any, TCtx IProcedureContext](p *Procedure[TCtx]) []TEvent {
	events := make([]TEvent, 0)
	for _, event := range p.Events {
		if specificEvent, ok := event.(TEvent); ok {
			events = append(events, specificEvent)
		}
	}
	return events
}

func GetAndRemoveEvents[TEvent any, TCtx IProcedureContext](p *Procedure[TCtx]) (TEvent, bool) {
	events := make([]interface{}, 0)
	var target TEvent
	isOk := false
	for _, event := range p.Events {
		if specificEvent, ok := event.(TEvent); ok && !isOk {
			target = specificEvent
			isOk = true
			continue
		}
		events = append(events, event)
	}
	p.Events = events
	return target, isOk
}

func GetStructId(i interface{}) string {
	t := reflect.TypeOf(i)
	pkgPath := t.PkgPath()
	processName := t.Elem()
	return pkgPath + "." + processName.Name()
}
