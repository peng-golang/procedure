package procedure

import (
	"github.com/pkg/errors"
	"go.uber.org/zap"
)

type IProcedureContext interface {
	GetContextString() string
	GetContextId() string
	GetLogger() *zap.SugaredLogger
}

type ContextProcedure struct {
	Err    error
	Events []interface{}
	Logger *zap.SugaredLogger
	Stack  []string
}

type Procedure[TCtx IProcedureContext] struct {
	Ctx TCtx
	*ContextProcedure
}

func (p *Procedure[TCtx]) addEvent(event interface{}) {
	p.Events = append(p.Events, event)
}

func New[TCtx IProcedureContext](ctx TCtx) *Procedure[TCtx] {
	return &Procedure[TCtx]{
		Ctx: ctx,
		ContextProcedure: &ContextProcedure{
			Events: make([]interface{}, 0),
			Logger: ctx.GetLogger(),
			Stack:  make([]string, 0),
		},
	}
}

func (p *Procedure[TCtx]) Execute(process Process[TCtx]) *Procedure[TCtx] {
	if p.Err != nil {
		return p
	}
	p.Logger.Debugw("start process", "process", process.GetProcessId())
	p.Stack = append(p.Stack, process.GetProcessId())
	err := process.Process(p.Ctx, p.addEvent)
	if err != nil {
		p.Err = errors.WithMessagef(err, "process failed in '%s' [ctx: %s] [stack: %v]",
			process.GetProcessId(), p.Ctx.GetContextString(), p.Stack)
		return p
	}
	return p
}

type Process[TCtx IProcedureContext] interface {
	GetProcessId() string
	Process(ctx TCtx, eventFunc AddEventFunc) error
}
